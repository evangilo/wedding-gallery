import client from './client';

export const login = ({ email, password }) =>
  client.post('/api/v1/auth/login', { email, password });

export const getUser = () =>
  client.get('/api/v1/user/me');

export const createUser = ({ name, email, password }) => {
  return client.post('/api/v1/user', { name, email, password });
}
