import axios from 'axios';

import { getToken, clearToken } from '../auth';

axios.interceptors.request.use(async config => {
  const token = getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

axios.interceptors.response.use(response => response, error => {
  if (error.response.status == 401) {
    clearToken();
    if (window.location.pathname !== '/login') {
      window.location = '/login';
    }
  }
  return Promise.reject(error);
})

export default axios;
