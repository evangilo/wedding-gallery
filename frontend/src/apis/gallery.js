import client from './client';

export const list = () =>
  client.get('/api/v1/gallery');

export const show = (galleryId) =>
  client.get(`/api/v1/gallery/${galleryId}`);

export const create = (data) =>
  client.post('/api/v1/gallery', data);

export const listPhotos = ({ galleryId, pending }) => {
  return client.get('/api/v1/photo', { params: { gallery_id: galleryId, pending }})
}

export const addPhotos = (data) => {
  const config = { headers: { 'Content-Type': 'multipart/form-data' } };
  return client.post('/api/v1/photo', data, config);
}

export const likePhoto = ({ photoId }) => {
  return client.post(`/api/v1/photo/${photoId}/like`, { photoId });
}

export const unlikePhoto = ({ photoId }) => {
  return client.post(`/api/v1/photo/${photoId}/unlike`, { photoId });
}

export const publishPhoto = ({ photoId }) => {
  return client.post(`/api/v1/photo/${photoId}/publish`);
}

export const deletePhoto = ({ photoId }) => {
  return client.delete(`/api/v1/photo/${photoId}`);
}
