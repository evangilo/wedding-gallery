import client from './client';

export const uploadPhotos = ({ photos, gallery_id }) =>
  client.post('/api/v1/photo', { photos, gallery_id });
