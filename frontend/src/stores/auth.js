import { observable, action, computed } from "mobx"
import { toast } from 'react-toastify';
import { get } from 'lodash';
import { getToken, saveToken, clearToken } from '../auth';
import * as userAPI from '../apis/user';

class Auth {
  @observable _token = null;

  constructor(token) {
    this._token = token;
  }

  @action login(credentials) {
    return userAPI.login(credentials)
      .then(({ data }) => { this._token = saveToken(data.access_token) })
      .catch(error => {
        toast.error(get(error, 'response.data.msg', 'Failed to do login'))
        return Promise.reject(error);
      })
  }

  @action createUser(data) {
    return userAPI.createUser(data)
      .then(() => toast.success('User created successfully'))
      .catch(error => toast.error('Failed to create user'))
  }

  @action logout() {
    clearToken();
    this._token = null;
  }

  @computed get isAuthenticated() {
    return !!this._token;
  }
}

export default new Auth(getToken());
