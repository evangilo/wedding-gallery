import { observable, action, computed } from "mobx"
import * as galleryAPI from '../apis/gallery';
import { toast } from 'react-toastify';
import { orderBy } from 'lodash';

class Gallery {
  @observable galleries = [];
  @observable gallery = {};
  @observable _photos = [];
  @observable photoOrderBy = 'likes';

  @computed get photos() {
    return orderBy(this._photos, this.photoOrderBy, 'desc');
  }

  @action sortPhotosBy(key) {
    this.photoOrderBy = key;
  }

  @action fetchGalleries() {
    galleryAPI.list()
      .then(({ data }) => { this.galleries = data.data })
      .catch(error => toast.error('Failed to retrieve the galleries'))
  }

  @action fetchGallery(galleryId) {
    galleryAPI.show(galleryId)
      .then(({ data }) => { this.gallery = data.data })
      .catch(error => toast.error('Failed to retrieve the gallery')
    );
  }

  @action fetchPhotos(galleryId, pending) {
    galleryAPI.listPhotos({ galleryId, pending }).then(({ data }) => {
      this._photos = data.data;
    }).catch(error => toast.error('Failed to retrieve the photos'));
  }

  @action createGallery(data) {
    galleryAPI.create(data).then(() => this.fetchGalleries());
  }

  @action addPhotos(data) {
    galleryAPI.addPhotos(data)
      .then(() => this.fetchPhotos(this.gallery.id))
      .then(() => toast.success('Photo added successfuly! Waiting by couple approval to be public'));
  }

  @action likeAction(photoId, action) {
    action == 'unlike' ? this.unlikePhoto(photoId) : this.likePhoto(photoId);
  }

  @action likePhoto(photoId) {
    galleryAPI.likePhoto({ photoId })
      .then(() => this.fetchPhotos(this.gallery.id))
      .catch(() => toast.error('Failed to like photo'));
  }

  @action unlikePhoto(photoId) {
    galleryAPI.unlikePhoto({ photoId })
      .then(() => this.fetchPhotos(this.gallery.id))
      .catch(() => toast.error('Failed to unlike photo'));
  }

  @action publishPhoto(photoId) {
    return galleryAPI.publishPhoto({ photoId })
      .then(() => this.fetchPhotos(null, true))
      .catch(() => toast.error('Failed to publish photo'));
  }

  @action deletePhoto(photoId) {
    return galleryAPI.deletePhoto({ photoId })
      .then(() => this.fetchPhotos(null, true))
      .catch(() => toast.error('Failed to delete photo'));
  }
}

export default new Gallery();
