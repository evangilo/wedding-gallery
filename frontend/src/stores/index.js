import gallery from './gallery';
import user from './user';
import auth from './auth';

export default {
  gallery,
  user,
  auth
};
