import { observable, action } from "mobx"
import * as userAPI from '../apis/user';
import { saveToken, clearToken } from '../auth';

class User {
  @action login(credentials) {
    return userAPI.login(credentials).then(({ data }) => {
      saveToken(data.access_token);
    });
  }

  @action logout() {
    clearToken();
  }
}

export default new User();
