export const isAuthenticated = () => !!getToken();

export const getToken = () => localStorage.getItem('token');

export const saveToken = (token) => {
  localStorage.setItem('token', token)
  return token;
}

export const clearToken = () => delete localStorage["token"];
