import React from 'react';
import { Provider } from 'mobx-react';
import { BrowserRouter, Link } from "react-router-dom";
import styled from 'styled-components';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { clearToken } from './auth';

import history from './history';
import stores from './stores';
import Routes from './screens/Routes';
import Authorized from './screens/Authorized';
import Unauthorized from  './screens/Unauthorized';
import LogoutButton from './screens/LogoutButton';

import Header from './components/Header';
import HeaderItem from './components/HeaderItem';
import HeaderBrand from './components/HeaderBrand';

import './App.css';

const Content = styled.div`
  padding: 0 16px;
`;

class App extends React.Component {
  logout = () => {
    this.props.auth.logout();
    history.push('/signin');
  }

  render() {
    return (
      <Provider {...stores}>
        <BrowserRouter history={history}>
          <div className="App">
            <Header>
              <HeaderBrand link="/" />
              <div>
                <Unauthorized>
                  <HeaderItem><Link to="/login">Log in</Link></HeaderItem>
                  <HeaderItem><Link to="/signup">Sign Up</Link></HeaderItem>
                </Unauthorized>
                <Authorized>
                  <HeaderItem><Link to="/pending">Pending</Link></HeaderItem>
                  <HeaderItem>
                    <LogoutButton />
                  </HeaderItem>
                </Authorized>
              </div>
            </Header>
            <Content>
              <Routes />
            </Content>
            <ToastContainer />
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
