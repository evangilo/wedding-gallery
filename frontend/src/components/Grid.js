import React from 'react';
import styled from 'styled-components';


const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  grid-auto-columns: min-content;
  grid-column-gap: 10px;
  grid-auto-rows: minmax(100px, auto);
  grid-row-gap: 1em;
  grid-auto-flow: column;
`

export default Grid;
