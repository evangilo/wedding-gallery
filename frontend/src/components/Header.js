import React from 'react';
import styled from 'styled-components';

const Header = styled.div`
  display: flex;
  height: 60px;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  align-items: center;
  justify-content: space-between;
  padding: 0 16px;
`;

export default Header;
