import React from 'react';
import styled from 'styled-components';

const Input = styled.input`
  border-radius: 3px;
  margin: 0.5em 1em;
  padding: 0.25em 1em;
  height: 22px;

  :focus {
    border: 2px solid palevioletred;
    outline-color: palevioletred;
  }
`;


export default Input;
