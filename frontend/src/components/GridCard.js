import React from 'react';
import styled from 'styled-components';

const GridCard = styled.div`
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s
  border-radius: 8px;
  min-width: 100px;
  overflow: hidden;

  :hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
  }
`;

export default GridCard;
