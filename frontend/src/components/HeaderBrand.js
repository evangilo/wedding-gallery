import React from 'react';
import { Link } from "react-router-dom";
import styled from 'styled-components';

const HeaderBrandWrapper = styled.div`
  a {
    text-decoration: none;
    color: black;
    font-weight: bold;
  }
`;

const HeaderBrand = ({ link }) => (
  <HeaderBrandWrapper>
    <Link to={link}>Wedding Gallery</Link>
  </HeaderBrandWrapper>
);

export default HeaderBrand;
