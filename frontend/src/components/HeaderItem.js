import React from 'react';
import styled from 'styled-components';

const HeaderItem = styled.span`
  padding: 0 8px;
  a {
    color: palevioletred;
    text-decoration: none;
  }
`
export default HeaderItem;
