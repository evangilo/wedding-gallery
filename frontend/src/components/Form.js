import React from 'react';

const formDataToJson = (formData) => (
  Array.from(formData.entries()).reduce((acc, [name, value]) => {
    if (name in acc) {
      const current = acc[name];
      acc[name] = Array.isArray(current) ? [...current, value] : [current, value];
    } else {
      acc[name] = value;
    }
    return acc;
  }, {})
);

const submit = (onSubmit, enctype) => (event ) => {
  event.preventDefault();
  const formData = new FormData(event.target);
  const shouldConvertToJson = enctype == 'application/json';
  const values = shouldConvertToJson ? formDataToJson(formData) : formData;
  return onSubmit(values);
};

const Form = ({ onSubmit, children, enctype, ...props }) => (
  <form {...props} onSubmit={submit(onSubmit, enctype)}>
    {children}
  </form>
);

Form.defaultProps = {
  enctype: 'application/json'
};

export default Form;
