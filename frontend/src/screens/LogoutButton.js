import React from 'react';
import { inject, observer } from 'mobx-react';
import history from '../history';


@inject('auth')
@observer
class LogoutButton extends React.Component {
  doLogout = () => {
    this.props.auth.logout();
    history.push('/signin');
  }

  render() {
    return <a href="#" onClick={this.doLogout}>Log out</a>
  }
}

export default LogoutButton;
