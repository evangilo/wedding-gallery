import React from 'react';
import { inject, observer } from 'mobx-react';

const Authorized = inject('auth')(observer(({ children, auth }) =>
  auth.isAuthenticated ? children : null
));

export default Authorized;
