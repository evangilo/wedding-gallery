import React from 'react';
import { inject, observer } from 'mobx-react';

import Form from '../../components/Form';

@inject('auth')
class SignUpForm extends React.Component {
  createUser = (data) => {
    const { auth, history } = this.props;
    auth.createUser(data).then(() => history.push('/login'));
  }

  render() {
    return (
      <div>
        <h1>Sign Up</h1>
        <Form onSubmit={this.createUser}>
          <input type="text" name="name" required placeholder="Name" />
          <input type="email" name="email" required placeholder="Email" />
          <input type="password" name="password" minlength="8" required placeholder="Password" />
          <button type="submit">Create</button>
        </Form>
      </div>
    )
  }
}

export default SignUpForm;
