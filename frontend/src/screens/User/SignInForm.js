import React from 'react';
import { inject, observer } from 'mobx-react';

import Form from '../../components/Form';
import { login } from '../../apis/user';
import { saveToken } from '../../auth';

@inject('auth')
class SignInForm extends React.Component {
  doLogin = (credentials) => {
    this.props.auth.login(credentials)
      .then(() => this.props.history.push('/'));
  }

  render() {
    return (
      <div>
        <h1>Sign In</h1>
        <Form onSubmit={this.doLogin}>
          <input type="email" name="email" required placeholder="Email" />
          <input type="password" name="password" required placeholder="Password" />
          <button type="submit">Login</button>
        </Form>
      </div>
    );
  }
}

export default SignInForm;
