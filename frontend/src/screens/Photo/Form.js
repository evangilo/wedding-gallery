import React from 'react';
import Form from '../components/Form';
import { uploadPhotos } from '../apis/photo';

const PhotoForm = () => (
  <Form onSubmit={uploadPhotos}>
    <input type="file" name="photos" multiple />
    <button type="submit">Send</button>
  </Form>
);

export default PhotoForm;
