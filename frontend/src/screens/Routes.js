import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import { isAuthenticated } from '../auth';

import ScreenUserSignIn from './User/SignInForm';
import ScreenUserSignUp from './User/SignUpForm';
import ScreenGalleryList from './Gallery/Gallery';
import ScreenGalleryDetail from './Gallery/Detail';
import ScreenGalleryPending from './Gallery/Pending';
import ScreenNotFound from './NotFound';


const PrivateRoute = ({ component: Component, ...props }) => (
  <Route
    {...props}
    render={routeProps =>
      isAuthenticated() ? (
        <Component {...routeProps} />
      ) : (
        <Redirect to={{ pathname: '/signin', state: { from: routeProps.location } }} />
      )
    }
  />
);

const Routes = () => (
  <Switch>
    <Route exact path="/" component={ScreenGalleryList} />
    <Route exact path="/gallery/:galleryId" component={ScreenGalleryDetail} />
    <Route exact path="/pending" component={ScreenGalleryPending} />
    <Route exact path="/login" component={ScreenUserSignIn} />
    <Route exact path="/signup" component={ScreenUserSignUp} />
    <Route path="*" component={ScreenNotFound} />
  </Switch>
);

export default Routes;
