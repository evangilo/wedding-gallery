import React from 'react';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { CSSGrid, measureItems, makeResponsive, layout, SpringGrid } from 'react-stonecutter';

import Form from '../../components/Form';
import GridCard from '../../components/GridCard';
import Button from '../../components/Button';

import HeartImage from '../../assets/heart.png';
import HeartOutlineImage from '../../assets/heart-outline.png';

const Grid = makeResponsive(measureItems(CSSGrid), {
  maxWidth: 1920,
  minPadding: 100,
  measureImages: true
});

const GridList = styled.ul`
  margin: 0 auto;
  padding: 0;
  li {
    list-style-type: none;
  }
`

const CardWrapper = styled.figure`
  background: #fefefe;
	border: 2px solid #fcfcfc;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
	margin: 16px 0;
	padding: 0;
	transition: opacity .4s ease-in-out;
  display: inline-block;
  column-break-inside: avoid;

  :hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
  }

`

const Image = styled.img`
  width: 300px;
  height: auto;
  min-width: 300px;
  padding: 0;
  margin: 0;
  object-fit: cover;
`

const LikeWrapper = styled.div`
  cursor: pointer;
  display: flex;
  padding: 8px 16px ;
  align-items: center;
`;

const LikeImage = styled.img`
  width: 32px;
  margin-right: 8px;
`

const LikeSpan = styled.span`
  font-weight: bold;
  color: palevioletred;
  margin-right: 4px;
`

const HelpText = styled.p`
  text-align: left;
  margin: 8px 16px;
  font-size: 12px;
`;

const SubHeader = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 16px;
  border-bottom: 1px solid rgba(0,0,0,0.2);
  margin-left: -16px;
  margin-right: -16px;
  margin-bottom: 8px;
`;

const SortForm = styled.div`
  display: flex;
  align-items: center;

  input {
    margin: 0 8px;
  }
`;

const Photo = ({ id, filename, url, published, likes = 0, liked, onLike }) => {
  const action = liked ? 'unlike' : 'like';
  return (
    <CardWrapper>
      <Image src={url} onDoubleClick={() => onLike(id, action)}/>
      <LikeWrapper onClick={() => onLike(id, action)}>
        <LikeImage
          src={liked ? HeartImage : HeartOutlineImage}
        />
        <LikeSpan>Likes:</LikeSpan><span>{likes}</span>
      </LikeWrapper>
      {published ? null : (<HelpText>{'Waiting approval'}</HelpText>)}
    </CardWrapper>
  );
};


@inject('gallery')
@observer
class GalleryDetail extends React.Component {
  componentDidMount() {
    const { gallery, match } = this.props;
    const { galleryId } = match.params;
    gallery.fetchGallery(galleryId);
    gallery.fetchPhotos(galleryId);
  }

  render() {
    const { gallery, match } = this.props;
    const { galleryId } = match.params;
    const { photos = [] } = gallery;

    return (
      <div>
        <SubHeader>
          <SortForm>
            <p>Sort by:</p>
            <input
              type="radio"
              name="sort_by"
              defaultValue="created_at"
              checked={gallery.photoOrderBy == 'created_at'}
              onChange={e => gallery.sortPhotosBy(e.target.value)}
              id="sort-date"
            />
            <label for="sort-date">{'Date'}</label>

            <input
              type="radio"
              name="sort_by"
              defaultValue="likes"
              checked={gallery.photoOrderBy == 'likes'}
              onChange={e => gallery.sortPhotosBy(e.target.value)}
              id="sort-likes"
            />
            <label for="sort-likes">{'Likes'}</label>
          </SortForm>
          <Form
            onSubmit={values => gallery.addPhotos(values)}
            enctype="multipart/form-data"
          >
            <input type="hidden" name="gallery_id" defaultValue={galleryId} />
            <input type="file" name="photos" multiple required />
            <Button primary type="submit">Add Photos</Button>
          </Form>
        </SubHeader>
        <Grid
          component={GridList}
          columns={4}
          columnWidth={300}
          gutterWidth={40}
          gutterHeight={0}
          layout={layout.pinterest}
          duration={300}
          easing="ease-out"
        >
          {photos.map(photo => (
            <li key={photo.filename}>
              <Photo
                {...photo}
                onLike={gallery.likeAction.bind(gallery)}
              />
            </li>
          ))}
        </Grid>
      </div>
    );
  }
}

export default GalleryDetail;
