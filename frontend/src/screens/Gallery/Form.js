import React from 'react';
import Form from '../components/Form';
import { createGallery } from '../apis/gallery';

const GalleryForm = () => (
  <Form onSubmit={createGallery}>
    <input type="text" name="name" required placeholder="Name" />
    <button type="submit">Create</button>
  </Form>
);

export default GalleryForm;
