import React from 'react';
import { inject, observer } from 'mobx-react';
import styled from 'styled-components';

import Form from '../../components/Form';
import Grid from '../../components/PhotoGrid';
import Button from '../../components/Button';
import Input from '../../components/Input';
import GalleryList from './List';
import Authorized from '../Authorized';


const GalleryFormSection = styled.div`
  display: flex;
  height: 60px;
  align-items: center;
  justify-content: flex-end;
`;

const FormWrapper = styled.div`
  display: flex;
  padding: 16px;
  justify-content: flex-end;
  border-bottom: 1px solid rgba(0,0,0,0.2);
  margin-left: -16px;
  margin-right: -16px;
  margin-bottom: 16px;
`;

const GalleryWrapper = styled.div`
  margin-top: 16px;
`

const GalleryCreateForm = ({ onCreate }) => (
  <FormWrapper>
    <Form onSubmit={onCreate}>
      <Input name="name" placeholder="Gallery name" required />
      <Input type="email" name="husband_email" placeholder="Husband email" required />
      <Input type="email" name="wife_email" placeholder="Wife emal" required />
      <Button primary type="submit">Add Gallery</Button>
    </Form>
  </FormWrapper>
);

@inject('gallery')
@observer
class Gallery extends React.Component {
  componentWillMount() {
    this.props.gallery.fetchGalleries();
  }

  render() {
    const { galleries } = this.props.gallery;
    return (
      <GalleryWrapper>
        <Authorized>
          <GalleryCreateForm onCreate={values => this.props.gallery.createGallery(values)} />
        </Authorized>
        <GalleryList galleries={galleries} />
      </GalleryWrapper>
    );
  }
}

export default Gallery;
