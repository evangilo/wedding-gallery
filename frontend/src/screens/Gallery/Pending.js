import React from 'react';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { CSSGrid, measureItems, makeResponsive, layout, SpringGrid } from 'react-stonecutter';

import Form from '../../components/Form';
import GridCard from '../../components/GridCard';
import Button from '../../components/Button';

import HeartImage from '../../assets/heart.png';
import HeartOutlineImage from '../../assets/heart-outline.png';

const Grid = makeResponsive(measureItems(CSSGrid), {
  maxWidth: 1920,
  minPadding: 100,
  measureImages: true
});

const GridList = styled.ul`
  margin: 0 auto;
  padding: 0;
  li {
    list-style-type: none;
  }
`

const GridWrapper = styled.div`
	column-width: 300px;
	column-gap: 8px;
	max-width: 1600px;
  text-align: center;
`;

const CardWrapper = styled.figure`
  background: #fefefe;
	border: 2px solid #fcfcfc;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
	margin: 16px 0;
	padding: 0;
	transition: opacity .4s ease-in-out;
  display: inline-block;
  column-break-inside: avoid;

  :hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
  }

`

const Image = styled.img`
  width: 300px;
  height: auto;
  min-width: 300px;
  padding: 0;
  margin: 0;
  object-fit: cover;
`

const LikeWrapper = styled.div`
  display: flex;
  padding: 8px ;
  align-items: center;
`;

const LikeImage = styled.img`
  width: 32px;
  margin-right: 8px;
`

const LikeSpan = styled.span`
  font-weight: bold;
  color: palevioletred;
  margin-right: 4px;
`

const ActionButton = styled(Button)`
  width: 100%;
  margin: 8px;
  cursor: pointer;
  :hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
  }

`;

const Photo = ({ id, url, onPublish, onDelete }) => {
  return (
    <CardWrapper>
      <Image src={url} />
      <LikeWrapper>
        <ActionButton onClick={() => onDelete(id)}>Delete</ActionButton>
        <ActionButton primary onClick={() => onPublish(id)}>Publish</ActionButton>
      </LikeWrapper>
    </CardWrapper>
  );
};


@inject('gallery')
@observer
class GalleryPending extends React.Component {
  componentDidMount() {
    this.props.gallery.fetchPhotos(null, true);
  }

  render() {
    const { gallery } = this.props;
    const { photos = [] } = gallery;

    return (
      <div>
        <Grid
          component={GridList}
          columns={4}
          columnWidth={300}
          gutterWidth={40}
          gutterHeight={0}
          layout={layout.pinterest}
          duration={300}
          easing="ease-out"
        >
          {photos.map(photo => (
            <li key={photo.id}>
              <Photo
                {...photo}
                onPublish={gallery.publishPhoto.bind(gallery)}
                onDelete={gallery.deletePhoto.bind(gallery)}
              />
            </li>
          ))}
        </Grid>
      </div>
    );
  }
}

export default GalleryPending;
