import React from 'react';
import { inject } from 'mobx-react';
import { Link } from "react-router-dom";
import styled from 'styled-components';

import Grid from '../../components/Grid';
import GridCard from '../../components/GridCard';
import EmptyImg from '../../assets/empty.jpg';


const PhotoImg = styled.img`
  width: auto;
  height: 180px;
  object-fit: cover;
`

const GalleryInfo = styled.div`
  padding: 8px 16px 16px 16px;
`

const GalleryTitle = styled.h4`
  margin: 0;
  padding: 4px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const GallerySubTitle = styled.h6`
  margin: 0;
  padding: 4px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const GalleryItem = ({ id, name, created_by, isAuthenticated }) => (
  <GridCard>
    <Link to={isAuthenticated ? `/gallery/${id}`: '#'}>
      <PhotoImg src={EmptyImg} />
    </Link>
    <GalleryInfo>
      <GalleryTitle>{name}</GalleryTitle>
      <GallerySubTitle>{created_by.name}</GallerySubTitle>
    </GalleryInfo>
  </GridCard>
);

const GalleryList = inject('auth')((({ galleries, auth }) => {
  return (
    <Grid>
      {galleries.map(gallery => (
        <GalleryItem {...gallery} key={gallery.id} isAuthenticated={auth.isAuthenticated} />
      ))}
    </Grid>
  );
}));

export default GalleryList;
