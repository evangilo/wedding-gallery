import React from 'react';
import { inject, observer } from 'mobx-react';

const Unauthorized = inject('auth')(observer(({ children, auth }) =>
  auth.isAuthenticated ? null : children
));

export default Unauthorized;
