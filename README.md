# Wedding Gallery

## Setup


`pip install -r requirements.txt`

`pip install -e .`

`cd frontend && npm install`

`cp settings.cfg.sample settings.cfg` 


# Running backend server (Dev)

`docker run -d -p 27017:27017 mongo`

`export WEDDING_GALLERY_SETTINGS=$PWD/settings.cfg`

`python wsgi.py`

# Running frontend server (Dev)

`cd frontend && npm start`


# Running tests

`pip install -r test_requirements.txt`

`docker run -d -p 27017:27017 mongo`

`export WEDDING_GALLERY_SETTINGS=$PWD/test_settings.cfg`

`pytest tests -v`
