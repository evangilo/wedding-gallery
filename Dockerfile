FROM python:3

COPY . /app

WORKDIR /app

RUN pip install -r requirements.txt
RUN pip install -e .
RUN pip install uwsgi

ENV WEDDING_GALLERY_SETTINGS=/app/settings.cfg

CMD uwsgi --socket 0.0.0.0:5000 --protocol=http -w wsgi --callable app --enable-threads --master --process 4
