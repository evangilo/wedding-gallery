test:
	WEDDING_GALLERY_SETTINGS=$(PWD)/test_settings.cfg pytest --pycodestyle tests -vvv

pep8:
	pycodestyle --benchmark
