from flask import request, jsonify
from flask_jwt_extended import jwt_required

from .. import auth, model
from wedding_gallery import exceptions
from wedding_gallery.app import app


@app.route('/api/v1/auth/login', methods=['POST'])
def login():
    # TODO: validate request body
    data = request.json
    email = data.get('email')
    password = data.get('password')

    if not email:
        return jsonify(msg='Missing email'), 400
    if not password:
        return jsonify(msg='Missing password'), 400

    try:
        token = auth.authenticate(email, password)
    except exceptions.InvalidCredentials:
        return jsonify(msg='Invalid credentials'), 401

    return jsonify(access_token=token), 200
