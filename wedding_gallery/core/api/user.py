import pymodm
import pymongo

from flask import request, jsonify
from flask_jwt_extended import jwt_required

from .. import auth, model
from wedding_gallery.app import app


@app.route('/api/v1/user/me', methods=['GET'])
@jwt_required
def user_me():
    user = auth.get_logged_user()
    return jsonify({
        'name': user.name,
        'email': user.email
    })


@app.route('/api/v1/user', methods=['POST'])
def user_create():
    try:
        data = request.json
        user = model.User(
            name=data.get('name'),
            email=data.get('email'),
            password=data.get('password'),
            gender=data.get('gender')
        )
        user.save()
    except ValueError:
        return jsonify({'msg': 'Invalid JSON'}), 400
    except pymodm.errors.ValidationError as error:
        return jsonify({
            'msg': 'Bad request',
            'errors': error.message
        }), 400
    except pymongo.errors.DuplicateKeyError:
        return jsonify({
            'msg': 'Unprocessable entity',
            'errors': {'email': 'email already in use'}
        }), 422
    return jsonify({'msg': 'created'}), 201
