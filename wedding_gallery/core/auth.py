from flask_jwt_extended import create_access_token, get_jwt_identity
from werkzeug.security import check_password_hash

from .model import User
from wedding_gallery import exceptions


def authenticate(email: str, password: str) -> str:
    """If the email and password are correct will returned the access token"""
    user = User.get_by_email(email)
    is_valid_password = user and check_password_hash(user.password, password)
    if is_valid_password:
        return create_access_token(identity=user.email)
    raise exceptions.InvalidCredentials()


def get_logged_user() -> User:
    """Returns the logged user"""
    email = get_jwt_identity()
    return email and User.get_by_email(email)
