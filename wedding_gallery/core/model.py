import pymongo
from pymodm import MongoModel, fields, errors
from werkzeug.security import generate_password_hash

from wedding_gallery.base_model import BaseMixin


class PasswordField(fields.CharField):
    def to_mongo(self, value):
        return generate_password_hash(value)


class User(MongoModel, BaseMixin):
    email = fields.EmailField(required=True)
    name = fields.CharField(max_length=100, required=True)
    password = PasswordField(required=True, min_length=8)
    gender = fields.CharField(choices=('MALE', 'FEMALE', 'OTHER'), blank=True)

    class Meta:
        indexes = [
            pymongo.IndexModel([('email', 1)], unique=True)
        ]

    @classmethod
    def get_by_email(cls, email):
        try:
            return cls.objects.get({'email': email})
        except errors.DoesNotExist:
            return None
