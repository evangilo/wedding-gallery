import uuid
import os
from datetime import datetime

import pymongo

from pymodm import EmbeddedMongoModel, MongoModel, fields, errors

from wedding_gallery.app import app
from wedding_gallery.base_model import BaseMixin


class Gallery(MongoModel, BaseMixin):
    name = fields.CharField(min_length=3, max_length=255, required=True)
    husband_email = fields.EmailField(required=True)
    wife_email = fields.EmailField(required=True)
    created_by = fields.ReferenceField('User')

    @property
    def reviewer_emails(self):
        return [self.husband_email, self.wife_email]


class Photo(MongoModel, BaseMixin):
    filename = fields.CharField()
    published = fields.BooleanField(default=False)
    created_by = fields.ReferenceField('User')
    created_at = fields.DateTimeField(default=datetime.now)
    user_likes = fields.ListField(fields.ReferenceField('User'), blank=True)
    gallery = fields.ReferenceField('Gallery')

    class Meta:
        indexes = [
            pymongo.IndexModel([('filename', 1)], unique=True)
        ]

    def like(self, user):
        return Photo.objects.raw({'_id': self.pk}) \
            .update({'$addToSet': {'user_likes': user.pk}})

    def unlike(self, user):
        return Photo.objects.raw({'_id': self.pk}) \
            .update({'$pull': {'user_likes': user.pk}})

    def publish(self):
        self.published = True
        self.save()

    @classmethod
    def save_image(cls, image, storage) -> str:
        filename = f'{uuid.uuid4()}.png'
        storage.save(image, filename)
        return filename

    @classmethod
    def get_pending_photos(cls, user):
        """Returns the user pending photos"""
        galleries = Gallery.objects.raw({
            '$or': [
                {'wife_email': user.email},
                {'husband_email': user.email}
            ]
        }).only('pk')

        return cls.objects.raw({
            'gallery': {'$in': [g.pk for g in galleries]},
            'published': False
        })

    @classmethod
    def get_available_photos(cls, user, gallery=None):
        """Returns the available photo to user.
           If specified the gallery param the photos will be filtered by it
        """
        query = cls.objects.raw({
            '$or': [
                {'published': True},
                {'created_by': user.pk}
            ]
        })
        if gallery:
            query = query.raw({'gallery': gallery.pk})

        return query
