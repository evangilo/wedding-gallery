import uuid

import pymodm

from flask import request, jsonify
from flask_jwt_extended import jwt_required
from pymodm import errors

from .. import model
from wedding_gallery.core import auth, model as core_model
from wedding_gallery.app import app


def format_gallery(gallery):
    return {
        'id': str(gallery.pk),
        'name': gallery.name,
        'created_by': {
            'id': str(gallery.created_by.pk),
            'name': gallery.created_by.name
        },
    }


@app.route('/api/v1/gallery', methods=['POST'])
@jwt_required
def gallery_create():
    user = auth.get_logged_user()
    data = request.json

    try:
        gallery = model.Gallery(
            name=data.get('name'),
            husband_email=data.get('husband_email'),
            wife_email=data.get('wife_email'),
            created_by=user
        )
        gallery.save()
    except pymodm.errors.ValidationError as error:
        return jsonify({
            'msg': 'Bad request',
            'errors': error.message
        }), 400

    return jsonify({'msg': 'created'}), 201


@app.route('/api/v1/gallery', methods=['GET'])
def gallery_list():
    query_set = model.Gallery.objects.all().only('name', 'pk', 'created_by')
    galleries = list(map(format_gallery, query_set))
    return jsonify({'data': galleries})


@app.route('/api/v1/gallery/<gallery_id>', methods=['GET'])
@jwt_required
def gallery_show(gallery_id):
    try:
        gallery = model.Gallery.get(gallery_id)
    except errors.DoesNotExist:
        return jsonify({'msg': 'Gallery not found'}), 404
    return jsonify({'data': format_gallery(gallery)})
