from bson import objectid
from flask import request, jsonify
from flask_jwt_extended import jwt_required
from pymodm import errors

from .. import model
from wedding_gallery.core import auth
from wedding_gallery.app import app
from wedding_gallery.libs import file_storage


def format_photo(photo, user):
    return {
        'id': str(photo.pk),
        'filename': photo.filename,
        'url': f'/images/{photo.filename}',
        'likes': len(photo.user_likes),
        'liked': user in photo.user_likes,
        'published': photo.published,
        'created_at': photo.created_at.isoformat()
    }


@app.route('/api/v1/photo', methods=['GET'])
@jwt_required
def photo_list():
    user = auth.get_logged_user()
    gallery_id = request.args.get('gallery_id')
    pending = request.args.get('pending')

    if pending:
        photos = model.Photo.get_pending_photos(user)
    else:
        gallery = gallery_id and model.Gallery.get(gallery_id)
        photos = model.Photo.get_available_photos(user, gallery=gallery)

    formatted_photos = [format_photo(photo, user) for photo in photos]
    return jsonify({'data': formatted_photos}), 200


@app.route('/api/v1/photo', methods=['POST'])
@jwt_required
def photo_create():
    gallery_id = request.form.get('gallery_id')
    images = request.files.getlist('photos')
    user = auth.get_logged_user()

    try:
        gallery = model.Gallery.get(gallery_id)
    except errors.DoesNotExist:
        return jsonify({'msg': 'Gallery not found'}), 404

    storage = file_storage.DiskStorage(app.config['IMAGE_FOLDER'])

    for image in images:
        filename = model.Photo.save_image(image, storage)
        model.Photo(
            filename=filename,
            created_by=user,
            gallery=gallery
        ).save()

    return jsonify({'msg': 'Photos added'}), 201


@app.route('/api/v1/photo/<photo_id>', methods=['DELETE'])
@jwt_required
def photo_delete(photo_id):
    user = auth.get_logged_user()
    photo = model.Photo.get(photo_id)
    if user.email not in photo.gallery.reviewer_emails:
        return jsonify({'msg': "Forbidden"}), 403
    photo.delete()
    return jsonify({'msg': 'Ok'}), 200


@app.route('/api/v1/photo/<photo_id>/like', methods=['POST'])
@jwt_required
def photo_like(photo_id):
    user = auth.get_logged_user()
    photo = model.Photo.get(photo_id)
    photo.like(user)
    return jsonify({'msg': 'Ok'}), 200


@app.route('/api/v1/photo/<photo_id>/unlike', methods=['POST'])
@jwt_required
def photo_unlike(photo_id):
    user = auth.get_logged_user()
    photo = model.Photo.get(photo_id)
    photo.unlike(user)
    return jsonify({'msg': 'Ok'}), 200


@app.route('/api/v1/photo/<photo_id>/publish', methods=['POST'])
@jwt_required
def photo_publish(photo_id):
    user = auth.get_logged_user()
    photo = model.Photo.get(photo_id)
    if user.email not in photo.gallery.reviewer_emails:
        return jsonify({'msg': "Forbidden"}), 403

    photo.publish()
    return jsonify({'msg': 'Ok'}), 200
