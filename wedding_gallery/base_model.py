from bson import objectid
from pymodm import MongoModel


class BaseMixin:
    @classmethod
    def get(cls, model_id: str):
        """Find the model by id"""
        _id = objectid.ObjectId(model_id)
        return cls.objects.get({'_id': _id})
