import os

from werkzeug.datastructures import FileStorage


class DiskStorage:
    def __init__(self, base_dir):
        self.base_dir = base_dir

    def save(self, file: FileStorage, filename: str) -> str:
        os.makedirs(self.base_dir, exist_ok=True)
        path = os.path.join(self.base_dir, filename)
        file.save(path)
        return path
