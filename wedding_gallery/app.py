from flask import Flask
from flask_jwt_extended import JWTManager
from flask_pymongo import PyMongo
from pymodm.connection import connect as pymodm_connect


app = Flask(__name__, static_url_path='/tmp')
app.config.from_envvar('WEDDING_GALLERY_SETTINGS')
mongo = PyMongo(app)
jwt = JWTManager(app)

pymodm_connect(app.config['MONGO_URI'])


from wedding_gallery.core.api import *
from wedding_gallery.gallery.api import *


@app.route('/images/<image_name>')
def images(image_name):
    from flask import send_from_directory, abort
    try:
        return send_from_directory(app.config['IMAGE_FOLDER'],
                                   filename=image_name,
                                   as_attachment=True)
    except FileNotFoundError:
        abort(404)
