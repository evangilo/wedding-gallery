import pytest

from pymodm import errors as pymodm_erros

from wedding_gallery.app import app as flask_app, mongo
from wedding_gallery.core.model import User


@pytest.fixture
def app():
    yield flask_app
    mongo.db.client.drop_database('test')


def test_user_save(app):
    User(email='test@gmail.com',
         name='Test',
         password='12345678').save()

    user = User.objects.get({'email': 'test@gmail.com'})
    assert user.name == 'Test'
    assert user.email == 'test@gmail.com'
    assert user.password.startswith('pbkdf2:sha256:')
    assert user.gender is None

    with pytest.raises(pymodm_erros.DoesNotExist):
        assert User.objects.get({'email': 'test1@gmail.com'}) is None
