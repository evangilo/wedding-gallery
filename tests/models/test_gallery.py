import pytest

from pymodm import errors as pymodm_erros

from wedding_gallery.app import app as flask_app, mongo
from wedding_gallery.core.model import User
from wedding_gallery.gallery import model


@pytest.fixture
def app():
    yield flask_app
    mongo.db.client.drop_database('test')


def test_get_user_pending_photos(app):
    user = User(email='dev@gmail.com', name='dev', password='12345678').save()
    gallery1 = model.Gallery(
        name='My Gallery',
        husband_email='dev@gmail.com',
        wife_email='nathalia@gmail.com',
    ).save()
    gallery2 = model.Gallery(
        name='My Gallery 2',
        husband_email='francisco@gmail.com',
        wife_email='nathalia@gmail.com',
    ).save()

    model.Photo(filename='photo1.png',
                published=True,
                gallery=gallery1).save()
    model.Photo(filename='photo2.png',
                published=False,
                gallery=gallery1).save()
    model.Photo(filename='photo3.png',
                published=False,
                gallery=gallery2).save()

    photos = [p for p in model.Photo.get_pending_photos(user)]

    assert len(photos) == 1
    assert photos[0].filename == 'photo2.png'


def test_like(app):
    user = User(email='dev@gmail.com', name='dev', password='12345678').save()
    gallery = model.Gallery(
        name='My Gallery',
        husband_email='dev@gmail.com',
        wife_email='nathalia@gmail.com',
    ).save()

    photo = model.Photo(filename='photo1.png',
                        published=True,
                        gallery=gallery).save()
    photo.like(user)
    photo = model.Photo.get(photo.pk)
    assert len(photo.user_likes) == 1

    # assert that one person can't like twice
    photo.like(user)
    photo = model.Photo.get(photo.pk)
    assert len(photo.user_likes) == 1


def test_unlike(app):
    user = User(email='dev@gmail.com', name='dev', password='12345678').save()
    gallery = model.Gallery(
        name='My Gallery',
        husband_email='dev@gmail.com',
        wife_email='nathalia@gmail.com',
    ).save()

    photo = model.Photo(filename='photo1.png',
                        published=True,
                        gallery=gallery).save()
    photo.like(user)
    photo = model.Photo.get(photo.pk)
    assert len(photo.user_likes) == 1

    photo.unlike(user)
    photo = model.Photo.get(photo.pk)
    assert len(photo.user_likes) == 0
