import json
from io import BytesIO

import pytest
from PIL import Image

from wedding_gallery import core
from wedding_gallery.gallery import model
from wedding_gallery.app import app as flask_app, mongo


@pytest.fixture
def app():
    core.model.User(name='Dev',
                    email='dev@gmail.com',
                    password='12345678').save()
    yield flask_app
    mongo.db.client.drop_database('test')


@pytest.fixture
def api_request(app):
    client = app.test_client()

    def request(method, path, params=None, headers=None, token=None, **kw):
        headers = headers or {}
        content_type = kw.pop('content_type', 'application/json')
        if token:
            headers['Authorization'] = 'Bearer {}'.format(token)

        return client.open(
            method=method, path=path,
            query_string=params,
            headers=headers, content_type=content_type, **kw)
    yield request


def test_create_gallery(app, api_request):
    with app.app_context():
        token = core.auth.authenticate('dev@gmail.com', '12345678')

    gallery_name = 'My First Gallery'
    resp = api_request('POST', '/api/v1/gallery', json={
        'name': gallery_name,
        'husband_email': 'dev@gmail.com',
        'wife_email': 'maria@gmail.com'
    }, token=token)

    assert resp.status_code == 201
    assert resp.json == {'msg': 'created'}

    gallery = model.Gallery.objects.get({'name': gallery_name})
    assert gallery.name == gallery_name
    assert gallery.created_by.email == 'dev@gmail.com'


def test_create_gallery_without_token(api_request):
    resp = api_request('POST', '/api/v1/gallery', json={
        'name': 'My First Gallery'
    })

    assert resp.status_code == 401
    assert resp.json == {'msg': 'Missing Authorization Header'}


def test_list_gallery(api_request):
    user = core.model.User.get_by_email('dev@gmail.com')
    model.Gallery(name='My Weending',
                  created_by=user,
                  husband_email='dev@gmail.com',
                  wife_email='nat@gmail.com').save()
    resp = api_request('GET', '/api/v1/gallery')

    galleries = resp.json['data']

    assert resp.status_code == 200
    assert len(galleries) == 1
    assert galleries[0]['name'] == 'My Weending'
    assert galleries[0]['created_by']['name'] == 'Dev'


def test_upload_photo(app, api_request):
    with app.app_context():
        token = core.auth.authenticate('dev@gmail.com', '12345678')

    gallery = model.Gallery(
        name='My Weending',
        husband_email='dev@gmail.com',
        wife_email='nat@gmail.com').save()

    resp = api_request(
        method='POST',
        path=f'/api/v1/photo',
        token=token,
        content_type='multipart/form-data',
        data={
            'gallery_id': gallery.pk,
            'photos': [
                (BytesIO(b'FILE CONTENT'), 'foo.png'),
                (BytesIO(b'FILE CONTENT'), 'bar.png')
            ],
        }
    )

    assert resp.status_code == 201
    assert resp.json == {'msg': 'Photos added'}


def test_upload_photo_with_invalid_gallery(app, api_request):
    with app.app_context():
        token = core.auth.authenticate('dev@gmail.com', '12345678')

    resp = api_request(
        method='POST',
        path=f'/api/v1/photo',
        token=token,
        content_type='multipart/form-data',
        data={
            'gallery_id': '5df2b77c34feee5ebe52ac1f',
            'photos': [
                (BytesIO(b'FILE CONTENT'), 'foo.png'),
                (BytesIO(b'FILE CONTENT'), 'bar.png')
            ],
        }
    )

    assert resp.status_code == 404
    assert resp.json == {'msg': 'Gallery not found'}
