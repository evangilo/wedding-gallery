import json

import pytest

from wedding_gallery.app import app as flask_app, mongo


@pytest.fixture
def app():
    yield flask_app
    mongo.db.client.drop_database('test')


@pytest.fixture
def client(app):
    yield app.test_client()


@pytest.fixture
def api_request(client):
    def request(method, path, data=None, params=None, headers=None):
        # https://github.com/pallets/werkzeug/blob/master/werkzeug/test.py#L222
        return client.open(method=method, path=path,
                           json=data, query_string=params,
                           headers=headers, content_type='application/json')
    yield request


def test_create_user(api_request):
    resp = api_request('POST', '/api/v1/user', {
        'password': '12345678',
        'name': 'Test',
        'email': 'test@email.com'
    })
    assert resp.status_code == 201
    assert resp.json == {'msg': 'created'}


def test_login(api_request):
    api_request('POST', '/api/v1/user', {
        'name': 'Test',
        'password': '12345678',
        'email': 'test@gmail.com'
    })
    resp = api_request('POST', '/api/v1/auth/login', {
        'email': 'test@gmail.com',
        'password': '12345678'
    })
    assert resp.status_code == 200
    assert resp.json['access_token'] is not None


def test_user_me(api_request):
    api_request('POST', '/api/v1/user', {
        'name': 'Test',
        'password': '12345678',
        'email': 'test@gmail.com'
    })
    resp = api_request('POST', '/api/v1/auth/login', {
        'email': 'test@gmail.com',
        'password': '12345678'
    })
    token = resp.json['access_token']
    resp = api_request('GET', '/api/v1/user/me', headers={
        'Authorization': 'Bearer {}'.format(token)
    })
    assert resp.status_code == 200
    assert resp.json == {'name': 'Test', 'email': 'test@gmail.com'}


def test_invalid_credentials(api_request):
    resp = api_request('POST', '/api/v1/auth/login', {
        'email': 'test@gmail.com',
        'password': 'test'
    })
    assert resp.status_code == 401
    assert resp.json == {'msg': 'Invalid credentials'}
